let DUMMY_RESOURCES = [];

for (let i = 0; i < 5; i++) {
  DUMMY_RESOURCES.push({
    id: Math.random().toString(),
    isFolder: false,
    displayName: "Facebook",
    thumb:
      "https://lh4.googleusercontent.com/-qfFKsYZbyBQ/TYHwDPCeXgI/AAAAAAAAE9M/T0BTA6mZYDE/s1600/Facebook+icon+01.png",
  });
}
DUMMY_RESOURCES.push({
  id: Math.random().toString(),
  isFolder: true,
  displayName: "Social",
  thumb:
    "https://lh4.googleusercontent.com/-qfFKsYZbyBQ/TYHwDPCeXgI/AAAAAAAAE9M/T0BTA6mZYDE/s1600/Facebook+icon+01.png",
});

export default DUMMY_RESOURCES;
