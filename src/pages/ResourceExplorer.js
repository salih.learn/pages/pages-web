import {
  ArrowLeftOutlined,
  ExclamationCircleOutlined,
  FileAddOutlined,
  FolderAddOutlined,
} from "@ant-design/icons";
import {
  Button,
  Drawer,
  Dropdown,
  Layout,
  Menu,
  Modal,
  PageHeader,
} from "antd";
import { useEffect, useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import {
  buildTreeNode,
  createFolder,
  createWebResource,
  deleteResource,
  editWebResource,
  getResource,
  isFolder,
  isWebResource,
  renameFolder,
} from "../api-client/explorer";
import ResourceList from "../components/explorer/ResourceList";
import AddFolderForm from "../components/forms/AddFolderForm";
import AddWebResourceForm from "../components/forms/AddWebResourceForm";
import styles from "./ResourceExplorer.module.css";

const { Header, Content, Footer } = Layout;
const { confirm } = Modal;
const menuItemStyle = {
  padding: "1em 2em",
};

const STATUS = {
  PENDING: "pending",
  ERROR: "error",
  SUCCESS: "success",
  REDIRECT: "redirect",
};

const ACTIONS = {
  ADD_FOLDER: "add-folder",
  ADD_WEB_RESOURCE: "add-web-resource",
  RENAME_FOLDER: "rename-folder",
  EDIT_WEB_RESOURCE: "edit-web-resource",
};

function externalRedirect(redirectUrl) {
  window.location.href = redirectUrl;
}

const ResourceExplorer = (props) => {
  const [status, setStatus] = useState(STATUS.PENDING);
  const [refreshTrigger, setRefreshTrigger] = useState(false);
  const [drawerState, setDrawerState] = useState({
    visible: false,
    title: null,
    status: null,
    goal: null,
    formData: null,
  });
  const params = useParams();
  const history = useHistory();
  const pathorigin = window.location.origin;
  const pathprefix = `/u/${params.username}/p/${params.pageId}/r/`;
  const [treeNode, setTreeNode] = useState({
    _self: null,
    parentNode: null,
    childNodes: [],
  });

  // Rebuild tree if resource changes
  useEffect(() => {
    console.log("Refreshing resource...");
    setRefreshTrigger(false);
    getResource(params.resourceId)
      .then((resource) => {
        setTreeNode(buildTreeNode(resource));
        if (isFolder(resource)) {
          setStatus(STATUS.SUCCESS);
        } else if (isWebResource(resource)) {
          setStatus(STATUS.REDIRECT);
          externalRedirect(resource.matter.url);
        }
      })
      .catch((e) => {
        setStatus(STATUS.ERROR);
        console.log(e);
      });
  }, [refreshTrigger, params.resourceId]);

  function onBackHandler() {
    history.replace(`${pathprefix}${treeNode.parentNode.id}/`);
  }

  function onCloseExplorerHandler() {
    history.push(`../../`);
  }

  function openDrawer(title, goal, formData) {
    setDrawerState({
      visible: true,
      title,
      goal,
      formData,
    });
  }
  function addFolderHandler() {
    openDrawer("New Folder", ACTIONS.ADD_FOLDER);
  }

  function addWebResourceHandler() {
    openDrawer("New Web Resource", ACTIONS.ADD_WEB_RESOURCE);
  }

  async function onEditResourceHandler(resourceId) {
    const resource = await getResource(resourceId);
    if (isFolder(resource)) {
      openDrawer("Rename Folder", ACTIONS.RENAME_FOLDER, {
        id: resource.id,
        name: resource.name,
      });
    } else if (isWebResource(resource)) {
      openDrawer("Edit link", ACTIONS.EDIT_WEB_RESOURCE, {
        id: resource.id,
        name: resource.name,
        url: resource.matter.url,
      });
    }
  }

  function showDeleteConfirm(resource) {
    const modalContent = isFolder(resource)
      ? `The directory ${resource.name} and all its contents will be erased permanantly`
      : `The resource ${resource.name} will be erased permanantly`;

    const { username, pageId } = params;
    confirm({
      title: `Are you sure about deleting ${resource.name}?`,
      icon: <ExclamationCircleOutlined />,
      content: modalContent,
      okText: "Yes",
      okType: "danger",
      cancelText: "No",
      onOk() {
        return deleteResource(username, pageId, resource.id).then(() => {
          setRefreshTrigger();
        });
      },
      onCancel() {},
    });
  }

  // eslint-disable-next-line
  async function onDeleteResourceHandler(resourceId) {
    const resource = await getResource(resourceId);
    showDeleteConfirm(resource);
  }

  function drawerCloseHandler() {
    setDrawerState({
      visible: false,
    });
  }

  async function onOpenResourceHandler(resourceId) {
    const resource = await getResource(resourceId);
    if (isWebResource(resource)) {
      window.location.href = resource.matter.url;
    }
    if (isFolder(resource)) {
      history.push(`${pathprefix}${resourceId}/`);
    }
  }

  function onCopyLinkHandler(resourceId) {
    navigator.clipboard.writeText(`${pathorigin}${pathprefix}${resourceId}`);
  }

  async function onCopyOriginalLinkHandler(resourceId) {
    const resource = await getResource(resourceId);
    if (isWebResource(resource)) {
      navigator.clipboard.writeText(resource.matter.url);
    }
  }
  function setDrawerPending() {
    setDrawerState({
      ...drawerState,
      status: STATUS.PENDING,
    });
  }

  function setDrawerError() {
    setDrawerState({
      ...drawerState,
      status: STATUS.ERROR,
    });
  }

  async function drawerFormSubmitHandler(data) {
    setDrawerPending();
    const { username, pageId, resourceId: parentId } = params;
    try {
      switch (drawerState.goal) {
        case ACTIONS.ADD_FOLDER: {
          const { name } = data;
          await createFolder(username, pageId, parentId, name);
          setRefreshTrigger(true);
          break;
        }
        case ACTIONS.ADD_WEB_RESOURCE: {
          const { name, url } = data;
          const body = { name, url };
          await createWebResource(username, pageId, parentId, body);
          setRefreshTrigger(true);
          break;
        }
        case ACTIONS.RENAME_FOLDER: {
          const { name } = data;
          const { id: resourceId } = drawerState.formData;
          await renameFolder(username, pageId, resourceId, name);
          setRefreshTrigger(true);
          break;
        }
        case ACTIONS.EDIT_WEB_RESOURCE: {
          const { name, url } = data;
          const { id: resourceId } = drawerState.formData;
          const body = { name, url };
          await editWebResource(username, pageId, resourceId, body);
          setRefreshTrigger(true);
          break;
        }
        default: {
          drawerCloseHandler();
          break;
        }
      }
      drawerCloseHandler();
    } catch (e) {
      console.log(e);
      setDrawerError();
    }
  }

  const newMenu = (
    <Menu>
      <Menu.Item
        onClick={addFolderHandler}
        style={menuItemStyle}
        key="folder"
        icon={<FolderAddOutlined />}
      >
        Folder
      </Menu.Item>
      <Menu.Item
        onClick={addWebResourceHandler}
        style={menuItemStyle}
        key="web resource"
        icon={<FileAddOutlined />}
      >
        Web Resource
      </Menu.Item>
    </Menu>
  );

  let jsx;

  if (status === STATUS.SUCCESS) {
    jsx = (
      <>
        <Layout className={styles.layout}>
          <Header className={styles.layoutHeader}>
            <PageHeader
              backIcon={<ArrowLeftOutlined style={{ color: "white" }} />}
              onBack={treeNode.parentNode ? onBackHandler : undefined}
              className={styles.header}
              title={
                <span style={{ color: "white" }}>{treeNode._self.name}</span>
              }
              extra={[
                <Button key="1" onClick={onCloseExplorerHandler}>
                  Close
                </Button>,
              ]}
            />
          </Header>
          <Content className={styles.layoutContent}>
            <ResourceList
              onDelete={onDeleteResourceHandler}
              onEdit={onEditResourceHandler}
              onCopyLink={onCopyLinkHandler}
              onCopyOriginalLink={onCopyOriginalLinkHandler}
              onOpen={onOpenResourceHandler}
              resourceList={treeNode.childNodes}
            />
          </Content>
          <Footer className={styles.layoutFooter}>
            <PageHeader
              className={styles.footer}
              extra={[
                <Dropdown
                  overlay={newMenu}
                  key="new-resource"
                  placement="bottomRight"
                >
                  <Button key="1" type="primary">
                    New
                  </Button>
                </Dropdown>,
              ]}
            />
          </Footer>
        </Layout>
        <Drawer
          title={drawerState.title}
          placement="bottom"
          closable={true}
          onClose={drawerCloseHandler}
          visible={drawerState.visible}
          height="60vh"
        >
          {drawerState.goal === ACTIONS.ADD_FOLDER && (
            <AddFolderForm
              status={drawerState.status}
              onFormSubmit={drawerFormSubmitHandler}
              submitText="Add"
            />
          )}
          {drawerState.goal === ACTIONS.ADD_WEB_RESOURCE && (
            <AddWebResourceForm
              status={drawerState.status}
              onFormSubmit={drawerFormSubmitHandler}
              submitText="Add"
            />
          )}
          {drawerState.goal === ACTIONS.RENAME_FOLDER && (
            <AddFolderForm
              status={drawerState.status}
              name={drawerState.formData.name}
              onFormSubmit={drawerFormSubmitHandler}
              submitText="Save"
            />
          )}
          {drawerState.goal === ACTIONS.EDIT_WEB_RESOURCE && (
            <AddWebResourceForm
              status={drawerState.status}
              name={drawerState.formData.name}
              url={drawerState.formData.url}
              onFormSubmit={drawerFormSubmitHandler}
              submitText="Save"
            />
          )}
        </Drawer>
      </>
    );
  }
  if (status === STATUS.PENDING) {
    jsx = <p>Loading...</p>;
  }
  if (status === STATUS.ERROR) {
    jsx = <p>Error occured!</p>;
  }
  if (status === STATUS.REDIRECT) {
    jsx = <p>Redirecting...</p>;
  }
  return jsx;
};

export default ResourceExplorer;
