import { Avatar, Card, Space, Typography } from "antd";
import { useEffect, useState } from "react";
import { useParams } from "react-router";
import { Link } from "react-router-dom";
import { getPage } from "../api-client/page";
import { LinkOutlined } from "@ant-design/icons";
import { Footer } from "antd/lib/layout/layout";
import { authPath } from "../route/lib";

const { Text } = Typography;

const PageHome = () => {
  const params = useParams();
  const { username, pageId } = params;
  const [page, setPage] = useState({
    name: null,
    rootResource: null,
    artifacts: [],
  });

  useEffect(() => {
    getPage(pageId).then((page) => {
      setPage(page);
    });
  }, [pageId]);

  return (
    <div
      style={{
        backgroundImage: "linear-gradient(aqua, black)",
        height: "100%",
        overflow: "auto",
      }}
    >
      <div
        style={{
          maxWidth: "1000px",
          minHeight: "60vh",
          padding: "150px",
          margin: "20vh auto",
          position: "relative",
          backgroundColor: "white",
        }}
      >
        <Typography>
          <Typography.Title>{page.name}</Typography.Title>
          {page.rootResource && (
            <Link to={`/u/${username}/p/${pageId}/r/${page.rootResource.id}/`}>
              Open Explorer
            </Link>
          )}
          {page.artifacts.map((artifact) => (
            <div key={artifact.id}>
              <Typography.Title level={3} style={{ margin: "2em 0 1em 0" }}>
                {artifact.title}
              </Typography.Title>
              <Space size={20} wrap>
                {artifact.items.map((item) => (
                  <a key={item.id} href={item.url}>
                    <Card style={{ width: 300 }} hoverable={true}>
                      <Card.Meta
                        avatar={
                          <Avatar
                            style={{
                              backgroundColor: "#1890ff",
                              cursor: "pointer",
                            }}
                            shape="square"
                            icon={<LinkOutlined />}
                          />
                        }
                        title={item.name}
                        description={item.url}
                      />
                    </Card>
                  </a>
                ))}
              </Space>
            </div>
          ))}
        </Typography>
      </div>
      <Footer>
        <Space wrap>
          <Text>Powered by Airnode Pages,</Text>
          <Link to={authPath}>Create an account / Sign in</Link>
        </Space>
      </Footer>
    </div>
  );
};

export default PageHome;
