import {
  LockOutlined,
  SolutionOutlined,
  UserOutlined,
} from "@ant-design/icons";
import {
  Button,
  Card,
  Checkbox,
  Divider,
  Form,
  Input,
  Space,
  Steps,
  Tabs,
} from "antd";
import GoogleAuthLink from "../components/auth/GoogleAuthLink";
const { TabPane } = Tabs;
const { Step } = Steps;
const AuthPage = () => {
  const onFinish = (values) => {
    console.log("Received values of form: ", values);
  };

  function callback(key) {
    console.log(key);
  }

  return (
    <div style={{ minHeight: "100%", backgroundColor: "#000B27" }}>
      <Card
        style={{
          width: "100%",
          maxWidth: "400px",
          margin: "auto",
          position: "relative",
          top: "10vh",
        }}
      >
        <Tabs defaultActiveKey="1" onChange={callback}>
          <TabPane tab="Sign In" key="1">
            <Form
              name="normal_login"
              className="login-form"
              initialValues={{
                remember: true,
              }}
              onFinish={onFinish}
            >
              <Form.Item
                name="email"
                rules={[
                  {
                    required: true,
                    message: "Please input your Email!",
                  },
                ]}
              >
                <Input
                  prefix={<UserOutlined className="site-form-item-icon" />}
                  placeholder="Email"
                />
              </Form.Item>
              <Form.Item
                name="password"
                rules={[
                  {
                    required: true,
                    message: "Please input your Password!",
                  },
                ]}
              >
                <Input
                  prefix={<LockOutlined className="site-form-item-icon" />}
                  type="password"
                  placeholder="Password"
                />
              </Form.Item>
              <Form.Item>
                <Space direction="vertical">
                  <Form.Item name="remember" valuePropName="checked" noStyle>
                    <Checkbox>Remember me</Checkbox>
                  </Form.Item>

                  <a className="login-form-forgot" href="">
                    Forgot password
                  </a>
                </Space>
              </Form.Item>

              <Form.Item>
                <Space>
                  <Button
                    type="primary"
                    htmlType="submit"
                    className="login-form-button"
                  >
                    Log in
                  </Button>
                </Space>
              </Form.Item>
            </Form>
            <Divider>OR</Divider>
            <GoogleAuthLink />
          </TabPane>
          <TabPane tab="Register" key="2">
            <Steps direction="vertical">
              <Step status="wait" title="Details" icon={<UserOutlined />} />
              <Form
                layout="vertical"
                name="normal_login"
                className="login-form"
                initialValues={{
                  remember: true,
                }}
                onFinish={onFinish}
              >
                <Form.Item
                  name="email"
                  rules={[
                    {
                      required: true,
                      message: "Please input your Email!",
                    },
                  ]}
                >
                  <Input
                    prefix={<UserOutlined className="site-form-item-icon" />}
                    placeholder="Email"
                  />
                </Form.Item>
                <Form.Item
                  name="password"
                  rules={[
                    {
                      required: true,
                      message: "Please input your Password!",
                    },
                  ]}
                >
                  <Input
                    prefix={<LockOutlined className="site-form-item-icon" />}
                    type="password"
                    placeholder="Password"
                  />
                </Form.Item>
                <Form.Item>
                  <Space>
                    <Button
                      type="primary"
                      htmlType="submit"
                      className="login-form-button"
                    >
                      Register
                    </Button>
                  </Space>
                </Form.Item>
              </Form>
              <Step
                status="wait"
                title="Verification"
                icon={<SolutionOutlined />}
              />
            </Steps>
            <Divider>OR</Divider>
            <GoogleAuthLink />
          </TabPane>
        </Tabs>
      </Card>
    </div>
  );
};

export default AuthPage;
