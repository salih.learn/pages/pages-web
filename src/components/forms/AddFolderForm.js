import { Form, Input, Button, Spin } from "antd";
import { useRef } from "react";

const AddFolderForm = (props) => {
  const nameInputRef = useRef();
  const isPending = props.status === "pending";

  function onSubmitHandler(event) {
    event.preventDefault();
    const data = {
      name: nameInputRef.current.input.value,
    };
    props.onFormSubmit(data);
  }

  return (
    <Form layout="vertical" onSubmit={onSubmitHandler}>
      <Form.Item label="Name">
        <Input
          defaultValue={props.name}
          ref={nameInputRef}
          placeholder="Give a name..."
        />
      </Form.Item>
      <Form.Item>
        {isPending && <Spin />}
        {!isPending && (
          <Button type="primary" onClick={onSubmitHandler} htmlType="submit">
            {props.submitText}
          </Button>
        )}
      </Form.Item>
    </Form>
  );
};

export default AddFolderForm;
