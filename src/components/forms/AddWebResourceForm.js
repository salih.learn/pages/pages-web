import { Form, Input, Button, Spin, Typography } from "antd";
import { useRef } from "react";
const { Text } = Typography;

const AddWebResourceForm = (props) => {
  const nameInputRef = useRef();
  const urlInputRef = useRef();
  const isPending = props.status === "pending";

  function onSubmitHandler(event) {
    event.preventDefault();
    const data = {
      name: nameInputRef.current.input.value,
      url: urlInputRef.current.input.value,
    };
    props.onFormSubmit(data);
  }

  return (
    <Form layout="vertical" onSubmit={onSubmitHandler}>
      <Form.Item label="Name">
        <Input
          defaultValue={props.name}
          ref={nameInputRef}
          placeholder="Give a name..."
        />
      </Form.Item>
      <Form.Item label="Resource URL">
        <Input
          defaultValue={props.url}
          ref={urlInputRef}
          placeholder="https://example.com/..."
        />
      </Form.Item>
      <Form.Item>
        {isPending && <Spin />}
        {!isPending && (
          <Button type="primary" onClick={onSubmitHandler} htmlType="submit">
            {props.submitText}
          </Button>
        )}
      </Form.Item>
      {props.status === "error" && <Text type="danger">Error occured</Text>}
    </Form>
  );
};

export default AddWebResourceForm;
