import {
  DeleteOutlined,
  EditOutlined,
  FolderOutlined,
  LinkOutlined,
  MoreOutlined,
  RightOutlined,
  ShareAltOutlined,
} from "@ant-design/icons";
import { Avatar, Button, Dropdown, List, Menu } from "antd";
import styles from "./Resource.module.css";

const menuItemStyle = {
  padding: "0.5em 1em",
};

const Resource = (props) => {
  const { isFolder } = props;
  const moreMenu = (
    <Menu>
      {isFolder && (
        <Menu.Item
          onClick={resourceClickHandler}
          style={menuItemStyle}
          key="open"
          icon={<FolderOutlined />}
        >
          Open Folder
        </Menu.Item>
      )}
      <Menu.Item
        onClick={onCopyLinkHandler}
        style={menuItemStyle}
        key="shareLink"
        icon={<ShareAltOutlined />}
      >
        Copy Link
      </Menu.Item>
      {!isFolder && (
        <Menu.Item
          onClick={onCopyOriginalLinkHandler}
          style={menuItemStyle}
          key="originalLink"
          icon={<LinkOutlined />}
        >
          Copy Original Link
        </Menu.Item>
      )}
      {!isFolder && (
        <Menu.Item
          onClick={onEditHandler}
          style={menuItemStyle}
          key="edit"
          icon={<EditOutlined />}
        >
          Edit
        </Menu.Item>
      )}
      {isFolder && (
        <Menu.Item
          onClick={onEditHandler}
          style={menuItemStyle}
          key="rename"
          icon={<EditOutlined />}
        >
          Rename
        </Menu.Item>
      )}
      <Menu.Divider />
      <Menu.Item
        onClick={onDeleteHandler}
        style={menuItemStyle}
        key="delete"
        icon={<DeleteOutlined />}
      >
        Delete
      </Menu.Item>
    </Menu>
  );

  const avatar = props.isFolder ? (
    <Avatar
      onClick={resourceClickHandler}
      style={{ backgroundColor: "orange", cursor: "pointer" }}
      shape="square"
      icon={<FolderOutlined />}
    />
  ) : (
    <Avatar
      onClick={resourceClickHandler}
      style={{ backgroundColor: "#1890ff", cursor: "pointer" }}
      shape="square"
      icon={<LinkOutlined />}
    />
  );

  const actions = [
    <span onClick={(ev) => ev.nativeEvent.stopPropagation()}>
      <Dropdown overlay={moreMenu} trigger={["click"]}>
        <Button>
          <MoreOutlined />
        </Button>
      </Dropdown>
    </span>,
  ];

  function resourceClickHandler(ev) {
    props.onOpen(props.id);
  }

  function onCopyLinkHandler() {
    props.onCopyLink(props.id);
  }

  function onEditHandler() {
    props.onEdit(props.id);
  }

  function onCopyOriginalLinkHandler() {
    props.onCopyOriginalLink(props.id);
  }

  function onDeleteHandler() {
    props.onDelete(props.id);
  }

  return (
    <List.Item
      actions={actions}
      style={{ padding: "12px" }}
      className={`${styles.resource}`}
    >
      <List.Item.Meta
        avatar={avatar}
        title={
          <span onClick={resourceClickHandler} style={{ cursor: "pointer" }}>
            {isFolder && <RightOutlined style={{ marginRight: "1em" }} />}
            {props.displayName}
          </span>
        }
      />
    </List.Item>
  );
};

export default Resource;
