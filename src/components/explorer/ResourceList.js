import { List } from "antd";
import Resource from "./Resource";
// import styles from "./ResourceList.module.css";

const ResourceList = (props) => {
  function onOpenResourceHandler(resourceId) {
    props.onOpen(resourceId);
  }

  function onCopyLinkHandler(resourceId) {
    props.onCopyLink(resourceId);
  }

  function onCopyOriginalLinkHandler(resourceId) {
    props.onCopyOriginalLink(resourceId);
  }

  function onEditResourceHandler(resourceId) {
    props.onEdit(resourceId);
  }

  function onDeleteResourceHandler(resourceId) {
    props.onDelete(resourceId);
  }

  return (
    <List style={{ padding: "1em", paddingBottom: "20vh" }}>
      {props.resourceList.map((res) => (
        <Resource
          onCopyOriginalLink={onCopyOriginalLinkHandler}
          onDelete={onDeleteResourceHandler}
          onCopyLink={onCopyLinkHandler}
          onOpen={onOpenResourceHandler}
          onEdit={onEditResourceHandler}
          displayName={res.name}
          isFolder={res.isFolder}
          id={res.id}
          key={res.id}
        />
      ))}
    </List>
  );
};

export default ResourceList;
