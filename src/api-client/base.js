const API_ROOT = "http://localhost:8080";
const postHeaders = {
  "Content-Type": "application/json",
};

export async function api(urlpath, method, body) {
  try {
    const response = await fetch(API_ROOT + urlpath, {
      method,
      headers: postHeaders,
      mode: "cors",
      body: body ? JSON.stringify(body) : undefined,
    });
    if (!response.ok) {
      throw response;
    }
    const data = await response.json();
    return data;
  } catch (e) {
    console.log(e);
    throw e;
  }
}

export async function get(urlpath) {
  return api(urlpath, "GET");
}

export async function post(urlpath, body) {
  return api(urlpath, "POST", body);
}

export async function patch(urlpath, body) {
  return api(urlpath, "PATCH", body);
}

export async function delete_req(urlpath) {
  return api(urlpath, "DELETE");
}
