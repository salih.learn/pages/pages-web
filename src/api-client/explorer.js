import { delete_req, get, patch, post } from "./base";

const MIMES = {
  FOLDER: "resource/directory",
  WEBURL: "resource/weburl",
};

export function isFolder(resource) {
  return resource.mime === "resource/directory";
}

export function isWebResource(resource) {
  return resource.mime === "resource/weburl";
}

export async function getResource(resourceId) {
  return await get(`/r/${resourceId}/`);
}

function extractNode(resource) {
  if (!resource) return null;
  const { id, name } = resource;
  return { id, name, isFolder: isFolder(resource) };
}

export function buildTreeNode(resource) {
  const treeNode = {
    _self: extractNode(resource),
    parentNode: extractNode(resource.parent),
    childNodes: resource.children.map((el) => {
      return extractNode(el);
    }),
  };

  return treeNode;
}

async function createResource(username, pageId, parentId, resource) {
  try {
    const response = await post(
      `/u/${username}/p/${pageId}/r/${parentId}`,
      resource
    );
    const { id: resourceId } = response;
    return await getResource(resourceId);
  } catch (e) {
    console.log(e);
    throw e;
  }
}

async function editResource(username, pageId, parentId, body) {
  try {
    const response = await patch(
      `/u/${username}/p/${pageId}/r/${parentId}`,
      body
    );
    const { id: resourceId } = response;
    return await getResource(resourceId);
  } catch (e) {
    console.log(e);
    throw e;
  }
}

export async function createFolder(username, pageId, parentId, name) {
  const resource = {
    mime: MIMES.FOLDER,
    name,
  };
  return await createResource(username, pageId, parentId, resource);
}

export async function renameFolder(username, pageId, parentId, name) {
  const requestBody = {
    name,
  };
  return await editResource(username, pageId, parentId, requestBody);
}

export async function editWebResource(username, pageId, parentId, body) {
  const requestBody = body;
  return await editResource(username, pageId, parentId, requestBody);
}

export async function createWebResource(username, pageId, parentId, body) {
  const { name, url } = body;
  const resource = {
    mime: MIMES.WEBURL,
    name,
    url,
  };
  return await createResource(username, pageId, parentId, resource);
}

export async function deleteResource(username, pageId, resrouceId) {
  return await delete_req(`/u/${username}/p/${pageId}/r/${resrouceId}`);
}
