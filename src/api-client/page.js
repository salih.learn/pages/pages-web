import { get } from "./base";

export async function getPage(pageId) {
  return await get(`/p/${pageId}`);
}
