export function pagePath(pageId) {
  return `/p/${pageId}/`;
}

export function resourcePath(resourceId) {
  return `/r/${resourceId}/`;
}

export function userDashboardPath() {
  return "/u/dashboard/";
}

export function authPath() {
  return "/auth/";
}

export const route_templates = {
  user_dashboard: userDashboardPath(),
  auth: authPath(),
  page: pagePath(":pageId"),
  resource: resourcePath(":resourceId"),
};
