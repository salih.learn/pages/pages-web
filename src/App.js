import { BrowserRouter as Router, Link, Route, Switch } from "react-router-dom";
import "./App.css";
import PageHome from "./pages/PageHome";
import ResourceExplorer from "./pages/ResourceExplorer";
import { authPath, userDashboardPath, route_templates } from "./route/lib";
import AuthPage from "./pages/AuthPage";
import UserDashboard from "./pages/UserDashboard";
import { Space } from "antd";

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/" exact>
          <Space direction="vertical">
            <Link to="/u/john/p/60f4035aa4d46164b23f7f50/r/60f4035aa4d46164b23f7f51/">
              Resource Explorer
            </Link>
            <Link to="/u/john/p/60f4035aa4d46164b23f7f50/" exact>
              Page
            </Link>
            <Link to={authPath()}>Sign up / Sign in</Link>
            <Link to={userDashboardPath()}>Dashboard</Link>
          </Space>
        </Route>
        <Route
          path="/u/:username/p/:pageId/r/:resourceId/"
          exact
          key="explorer"
        >
          <ResourceExplorer />
        </Route>
        <Route path="/u/:username/p/:pageId/" exact>
          <PageHome />
        </Route>
        <Route path={route_templates.auth} exact>
          <AuthPage />
        </Route>
        <Route path={route_templates.dashboard} exact>
          <UserDashboard />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
